# parser.py
import sys
import re
from pprint import pprint
from fractions import Fraction

M_FILE = "measures.txt"
I_FILE = "prepwords.txt"
CM_FILE = "cookingmethods.txt"
U_FILE = "utensils.txt"

def extract_ing(line):
    line = line.replace(" ,","")
    line = line.replace("()","")
    line = line.strip(',;: .')
    flag = False
    while flag == False:
        if line.endswith('and'):
            line = line[:len(line)-3].rstrip(',;: .')
        elif line.endswith('or'):
            line = line[:len(line)-2].rstrip(',;: .')
        else:
            flag = True

    while "  " in line:
        line = line.replace("  "," ")

    return line

def extract_prep(line):
    with open(I_FILE, 'r') as f:
        INGREDIENT_PREP  = [l.rstrip() for l in f.readlines()]
    ingred_prep = [re.findall(r'[a-zA-Z]+ly\s%s|%s\sinto\s[0-9a-zA-Z- ]*\s?(?:cubes|strips|chunks|pieces)|%s\s[0-9-/ ]*[a-zA-Z- ]*?\s?(?:thick|wide|long)|%s[ ,.;:?!]|%s$'%(word,word,word,word,word),line) for word in INGREDIENT_PREP]

    prep_list = [prep.strip() for term in ingred_prep for prep in term if prep.strip() !=""]

    prep_list = list(set(prep_list))

    prep_list = [phrase for phrase in prep_list if " ".join(prep_list).count(phrase) == 1]

    for phrase in prep_list:
        line = line.replace(phrase,"")

    return([prep_list,line])

def extract_quantities(line):
    n = re.search(r'\d*\s*\d\/\d{1,2}|\d+\.\d+|\d+\s',line)

    if n != None:
        number = float(sum(Fraction(s) for s in n.group(0).split()))
        line = re.sub(r'%s[ ,.:]'%n.group(0).strip(),"",line,count=1).lstrip()
    else:
        number = 1

    m = re.match(r'\(([0-9a-zA-Z-/ ]*)\)|(small)[,.?!;: ]|(large)[,.?!;: ]|(medium)[,.?!;: ]',line)

    if m != None:
        clarify = [match for match in m.groups() if match != None]
        line = line.replace(m.group(0),"")
    else:
        clarify = ""

    with open(M_FILE, 'r') as f:
        MEASURES  = [l.rstrip() for l in f.readlines()]

    measure_list = list(set([word for word in line.split() if word.lower() in MEASURES]))

    if len(measure_list) == 0:
        this_measure = ""
    else:
        this_measure = ' '.join(measure_list).strip()
        line = re.sub(r'%s'%this_measure,"",line, count=1)

    return([{'number': number, 'measure': this_measure, 'notes': clarify},line])

def extract_instructions(raw_instructions,ingredients,do_print):
    ing = [re.subn(r'[()*]*',"",i['ingredients'])[0] for i in ingredients]
    ing.append('water')
    instruct_list = re.split(r'[.?!;]',"".join(raw_instructions).lower().strip(' ().?!;:-'))

    with open(CM_FILE,'r') as f:
        COOKMETHODS = [l.rstrip() for l in f.readlines()]

    with open(U_FILE,'r') as f:
        UTENSILS = [l.rstrip() for l in f.readlines()]

    with open(M_FILE, 'r') as f:
        MEASURES  = [l.rstrip() for l in f.readlines()]

    instructions = []
    for line in instruct_list:
        line = line.strip(' (),:;')
        
        instruct_ings = list(set([i for i in ing if re.search(r' (%s[,.?!;: ]|%s$)'%(i,i),line)]))

        STOP_LIST = MEASURES + COOKMETHODS + UTENSILS + ['and','or','about','for','about','and/or','more']

        for i in ing:
            for j in [l.strip(' ,:;-.?!()') for l in i.split() if l.strip(' ,:;-.?!()').isalpha()]:
                if True not in [j in k for k in instruct_ings]:
                    search = re.search(r' (%s[,.?!;: ]|%s$)'%(j,j),line)
                    if re.search(r' (%s[,.?!;: ]|%s$)'%(j,j),line):
                        if j not in STOP_LIST:
                            instruct_ings.append(j)

        methods = list(set([m for m in COOKMETHODS if re.search(r' (%s[,.?!;: ]|%s$)'%(m,m),line)]))
        tools = list(set([u for u in UTENSILS if re.search(r' (%s[,.?!;: ]|%s$)'%(u,u),line)]))
        duration = re.findall(r'(?:about)?\s[0-9]+\s(?:minutes|minute|min\.|min|hours|hour|seconds|second|sec\.|sec)|until\s[a-zA-Z0-9 ]+',line)
        duration = [d.strip() for d in duration]
        instructions.append({
            'original': line,
            'ingredients': instruct_ings,
            'methods': methods,
            'utensils': tools,
            'duration': duration
        })

    if do_print:
        for count in range(0,len(instructions)):
            print instruct_list[count]
            pprint(instructions[count])


    return instructions

def extract_ingredients(raw_ingredients, do_print):
    results = []
    for line in raw_ingredients:
        if do_print:
            print "\n\nOriginal Line:"
            print line
        quantities, newline = extract_quantities(line)
        if do_print:
            print "Quantities:"
            pprint(quantities)
        prep, newline = extract_prep(newline)
        if do_print:
            print "Ingredient Prep:"
            pprint(prep)
        ingredients = extract_ing(newline)
        if do_print:
            print "Ingredient:"
        if " and " in ingredients:
            mini_ing_list = ingredients.split(' and ')
            if do_print:
                pprint(mini_ing_list)
            results.extend([{
            'quantities': quantities,
            'prep': prep,
            'ingredients': mini_ing_list[0]
            },{
            'quantities': quantities,
            'prep': prep,
            'ingredients': mini_ing_list[1]
            }])
        else:
            if do_print:
                print ingredients
            results.append({
                'quantities': quantities,
                'prep': prep,
                'ingredients': ingredients
                })

    return results

def parse(recipe_lines, do_print):
    raw = [l.strip() for l in recipe_lines]
    ingredients = extract_ingredients(raw[raw.index("Ingredients")
                               + 2:raw.index("Directions")-1], do_print)
    instructions = extract_instructions(raw[raw.index("Directions")+1:-1],ingredients, do_print)
    return(ingredients, instructions)


def main(fn = "RecipeExample.txt"):
    with open(fn,'r') as f:
        raw = [l.strip() for l in f.readlines()]
    return parse(raw, True)


if __name__ == '__main__':
    main()
