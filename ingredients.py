import json


class Ingredients(object):
    def __init__(self, filename=None):
        """
        Create a new Ingredients data structure. If `filename` is specified
        the data will be loaded from `filename`.
        """
        if filename:
            with open(filename) as f:
                self.data = json.load(f)
        else:
            self.data = {}

    def save(self, filename):
        """
        Save the data structure the `filename`, pretty printed.
        """
        with open(filename, 'w') as f:
            json.dump(self.data, f, indent=2)

    def add(self, ingredient, pos, neg):
        """
        Add a list of positive and negative attributes to the ingredient.
        strips, lower cases, and ensures uniqueness.

        >>> ingrs = Ingredients()
        >>> ingrs.add('parmesan cheese', ['italian', 'salty'], ['asian'])
        >>> ingrs.data
        {'parmesan cheese': {'pos': ['italian', 'salty'], 'neg': ['asian']}}
        >>> ingrs.add('parmesan cheese', ['cheesy', 'topping'], [])
        {'parmesan cheese': {'pos': ['italian', 'salty', 'cheesy', 'topping'],
            'neg': ['asian']}
        """
        ingredient = ingredient.strip().lower()
        pos = [attr.strip().lower() for attr in pos]
        neg = [attr.strip().lower() for attr in neg]
        ingr_attrs = self.data.get(ingredient, {})
        stored_pos = ingr_attrs.get('pos', [])
        stored_pos.extend(pos)
        ingr_attrs['pos'] = list(set(stored_pos))
        stored_neg = ingr_attrs.get('neg', [])
        stored_neg.extend(neg)
        ingr_attrs['neg'] = list(set(stored_neg))
        self.data[ingredient] = ingr_attrs

    def score(self, source_ingredient, desired, candidate):
        """
        Takes a linear combination of common attributes of different
        kinds to score how well a candidate ingredient would be as a
        substitute for the source_ingredient given the desired
        attributes.
        """
        desired = set(desired)
        source_pos = set(self.data[source_ingredient]['pos'])
        source_neg = set(self.data[source_ingredient]['neg'])
        cand_pos = self.data[candidate]['pos']
        cand_neg = self.data[candidate]['neg']
        return (0.5 * len(source_pos) * len(desired.intersection(cand_pos))
                + 1 * len(source_pos.intersection(cand_pos))
                - 50 * len(desired.intersection(cand_neg))
                - .1 * len(source_neg.intersection(cand_pos))
                - .1 * len(source_pos.intersection(cand_neg))
                + 1 * (source_ingredient == candidate))

    def translate(self, ingredient, desired):
        """
        Returns a list of ingredients sorted on how good of a substitute
        they would be for the given ingredient and the given desired
        attributes.
        ingredient - The ingredient to substitute.
        desired - A list of desired attributes
        returns - A sorted list of ingredients. The source ingredient
        may be in this list.

        >>> ings.translate('parmesan cheese', ['asian'])
        [u'salt', u'parmesan cheese']
        >>> ings.translate('parmesan cheese', ['cheesy'])
        [u'parmesan cheese', u'salt']
        >>> ings.translate('parmesan cheese', ['poop'])
        [u'parmesan cheese', u'salt']
        """
        ingredient = ingredient.lower()
        return sorted(self.data.keys(),
                      key=lambda i: -self.score(ingredient, desired, i))

    def __contains__(self, ingredient):
        return ingredient.lower() in self.data
