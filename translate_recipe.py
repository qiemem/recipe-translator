import parser
import ingredients as ing


def input_recipe():
    recipe = []
    print "\nCopy and paste a recipe\nType -1 on a new line at end of recipe"
    line = raw_input()
    while line != "-1":
        recipe.append(line + "\n")
        line = raw_input()
    
    return recipe
    


def do_translation(desired,filename):
    recipe = input_recipe()

    (ingredients,directions) = parser.parse(recipe, False)
    inglist = ing.Ingredients(filename)

    translated_ingredients = []

    for i in ingredients:
        if i["ingredients"] not in inglist:
            print ("\nIs \"" + i["ingredients"] + "\" the correct name of the " +
                   "ingredient? [y/n]:")
            correct = "a"
            while correct != "y" and correct != "n" and correct != "-1":
                correct = raw_input().lower()


            if correct == "n":
                new_name = ""
                while(not new_name):
                    new_name = raw_input("\nEnter the correct name of the " +
                                         "ingredient\nOr type -1 to " +
                                         "not include: ")

                if new_name == "-1":
                    ingredients.remove(i)
                    continue

                i["ingredients"] = new_name
                if new_name in inglist:
                    continue


            print ("\nEnter 1 attribute of " +  i["ingredients"] +
                  " per line.\nEnter a blank line when all attributes " +
                  "have been added.")

            pos_attributes = []
            attribute = raw_input()
            while attribute != "":
                pos_attributes.append(attribute)
                attribute = raw_input()

            print ("\nEnter 1 attribute that " +i["ingredients"] + " does not " +
                  "have per line.\nEnter a blank line when all negative " +
                  "attributes have been added.")

            neg_attributes = []
            attribute = raw_input()
            while attribute != "":
                neg_attributes.append(attribute)
                attribute = raw_input()

            inglist.add(i["ingredients"], pos_attributes, neg_attributes)
            inglist.save("ingredients.json")

        translated_ingredients.append((
            i,
            inglist.translate(i["ingredients"], desired)[0]
        ))

    inglist.save("ingredients.json")

    return (translated_ingredients, directions)


def main():
    desired = ""
    while not desired:
        desired = raw_input("What is your desired attribute? ")

    (translations,directions) = do_translation(desired,"ingredients.json")
    
    print "INGREDIENTS:"
    for t in translations:
        ingredient = "Quantity: %f"%t[0]['quantities']['number']
        if t[0]['quantities']['measure'] != "":
            ingredient += "\tMeasure: %s"%t[0]['quantities']['measure']
        if t[0]['quantities']['notes'] != "":
            ingredient += "\tNotes: %s"%t[0]['quantities']['notes']
        print ingredient
        prep = ""
        for p in t[0]['prep']:
            if len(t[0]['prep']) != 1:
                if t[0]['prep'][-1] == p:
                    prep += ", and "
                else:
                    prep += ","
            prep += p
        print "Preparation: %s"%prep
        print "Ingredient: %s --> %s"%(t[0]['ingredient'],t[1])

    print "DIRECTIONS:"
    pprint(directions)




if __name__ == '__main__':
    main()
