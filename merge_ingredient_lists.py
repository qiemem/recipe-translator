import ingredients as i


def main():
    print "Type .json filenames containing ingredient lists.\nType a blank line to stop entering filenames."
    filename = "a"
    names = []
    master_list = None

    while filename:
        filename = raw_input()
        if filename:
            names.append(filename)

    outname = raw_input("\nType the name of the output .json file: ")

    for filename in names:
        ingredients = i.Ingredients(filename)
        if master_list is None:
            master_list = ingredients
        else:
            for ing in ingredients.data:
                master_list.add(ing, ingredients.data[ing]["pos"], ingredients.data[ing]["neg"])

    master_list.save(outname)

    print "\nOutput file written.\n"




if __name__ == '__main__':
    main()
